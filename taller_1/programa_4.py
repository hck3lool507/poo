#!/usr/bin/env python3

#autor: Rafael Hernandez

nombre = input("Ingrese su nombre: ")
saldo = float(input("Ingrese el precio del producto: "))

saldo_total = (saldo * 0.07) + saldo

print("\nSu nombre es: {}".format(nombre))
print("Su monto a pagar es: {0:.2f}".format(saldo_total))
