#!/usr/bin/env python3

#autor: Rafael Hernandez

precio = float(input("Ingrese el precio del articulo adquirido: "))
codigo = int(input("Indique un codigo para articulo: "))
art_descript = input("Ingrese una descripcion para el articulo: ")

ganancia = (precio * 0.30) + precio

print("\nEl codigo del articulo es: {}".format(codigo))
print("La descripcion del articulo es: {}".format(art_descript))
print("El precio del articulo es: {0:.2f}".format(ganancia))
