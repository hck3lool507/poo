#!/usr/bin/env python3

#autor: Rafael Hernandez

registro_tiempo = []
dias = 5

nombre = input("Ingrese su nombre: ")
for t in range(dias):
    tiempo = float(input("Ingrese su tiempo del dia {} : ".format(t+1)))
    registro_tiempo.append(tiempo)

prom = sum(registro_tiempo) / dias

print("\nSu nombre es: {}".format(nombre))
print("Su tiempo promedio para {} dias es: {}".format(dias, prom))
print("Sus tiempos fueron: {}".format(registro_tiempo))
