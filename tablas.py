# Laboratorio 3
# author: Rafael Hernandez

import tkinter as tk


root = tk.Tk()
root.title("Tablas de Multiplicar")
canvas1 = tk.Canvas(root, width = 400, height = 300,  relief = 'raised')
canvas1.pack()

label1 = tk.Label(root, text='Calculate the multiplication tables')
label1.config(font=('helvetica', 14))
canvas1.create_window(200, 35, window=label1)

label2 = tk.Label(root, text='Type a number between 2-10:')
label2.config(font=('helvetica', 10))
canvas1.create_window(200, 100, window=label2)

entry1 = tk.Entry(root) 
canvas1.create_window(200, 140, window=entry1)


class Tables(object):
    def __init__(self, number):
        self.__number = number
        
    def imprimir_tabla(self):
        limit = 10
        counter = 1
        print("\n")
        if int(self.__number) >= 2 and int(self.__number) <= 10:
            while counter <= limit:
                result = counter * int(self.__number)
                print("{} x {} = {}".format(self.__number, counter, result))
                counter = counter + 1
        else:
            raise AssertionError("The number entered it's wrong")


button1 = tk.Button(text="Show Table", bg="orange red", command=lambda: Tables(entry1.get()).imprimir_tabla())
button2 = tk.Button(text="Clear Entry", bg="cyan", command=lambda: entry1.delete(0, "end"))
canvas1.create_window(150, 180, window=button1)
canvas1.create_window(250, 180, window=button2)

root.mainloop()

