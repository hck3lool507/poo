#!/usr/bin/env python3

"""autor: Rafael Hernandez"""

from perro import Dog
from gato import Cat

if __name__ == '__main__':
    pluto = Dog("pluto", "rawf rawf")
    tom = Cat("tom", "miau miau")
    print(pluto.show_info())
    print(pluto.bark())
    print(tom.show_info())
    print(tom.meaw())
