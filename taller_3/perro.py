#!/usr/bin/env python3

"""autor: Rafael Hernandez"""

from animales import Animals
import tkinter
from tkinter import messagebox


class Dog(Animals):
    def __init__(self, nombre, action):
        Animals.__init__(self, nombre, "dog")
        self.__action = action
        self.__nombre = nombre
        self.__root = tkinter.Tk()
        self.__root.withdraw()

    def bark(self):
        messagebox.showinfo("{}".format(self.__nombre), "says {}".format(self.__action))
