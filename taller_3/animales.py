#!/usr/bin/env python3

"""autor: Rafael Hernandez"""

import tkinter
from tkinter import messagebox


class Animals(object):
    def __init__(self, name, kind):
        self.__name = name
        self.__kind = kind
        self.__root = tkinter.Tk()
        self.__root.withdraw()

    def get_name(self):
        return self.__name

    def get_kind(self):
        return self.__kind

    def show_info(self):
        messagebox.showinfo("Info", "{} es un {}".format(self.__name, self.__kind))
