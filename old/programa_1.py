#!/usr/bin/env python3

#autor: Rafael Hernandez

class Terreno():
    def  __init__(self, ancho, longitud, costo_metro):
        self.__ancho = ancho
        self.__longitud = longitud
        self.__costo_metro = costo_metro


    def  calcular_costo(self):
        calculo = self.__ancho*self.__longitud
        costo = calculo*self.__costo_metro
        print(costo)


if __name__ == "__main__":
    costo_metro = float(input("Ingrese el costo del metro cuadrado: "))
    ancho = float(input("Ingrese el ancho del terreno en metros: "))
    longitud = float(input("Ingrese la longitud del terreno en metros: "))
    boquete = Terreno(costo_metro, ancho, longitud)
    boquete.calcular_costo()
