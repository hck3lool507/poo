#!/usr/bin/env python3

#autor: Rafael Hernandez

class Calificaciones():
    def  __init__(self, nombre, cantidad_calificaciones):
        self.__nombre = nombre
        self.__cant_calificaciones = cantidad_calificaciones
        self.__cal = []

    def  calcular_promedio(self):
        for x in range(self.__cant_calificaciones):
            calificacion = float(input("Ingrese la calificacion: "))
            self.__cal.append(calificacion)
        promedio = sum(self.__cal)/len(self.__cal)
        if promedio >= 71:
            print("Aprobado")
        else:
            print("Reprobado")

if __name__ == "__main__":
    nombre = input("Ingrese su nombre: ")
    cantidad_calificaciones = int(input("Ingrese la cantidad de calificaciones: "))
    estudiante1 = Calificaciones(nombre, cantidad_calificaciones)
    estudiante1.calcular_promedio()
