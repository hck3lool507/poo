#!/usr/bin/env python3

#autor: Rafael Hernandez

class Hotel():
    def  __init__(self, nombre, apellido, edad, habitacion, dias):
        self.__nombre = nombre
        self.__apellido = apellido
        self.__edad = edad
        self.__habitacion = habitacion
        self.__dias = dias

    def  descuento_hospedaje(self):
        descuento = 0
        subtotal = 0
        total = 0

        habitacion1 = 120.0
        habitacion2 = 155.0
        habitacion3 = 210.0
        habitacion4 = 185.0
        habitacion5 = 400

        if self.__habitacion == "1" and self.__dias > "5":
            descuento = (habitacion1*0.10) - habitacion1
        elif self.__habitacion == "1" and self.__dias > "10":
            descuento = (habitacion1*0.15) - habitacion1
        elif self.__habitacion == "1" and self.__dias > "15":
            descuento = (habitacion1*0.20) - habitacion1
        else:
            descuento = 0
            subtotal = habitacion1*self.__dias
            total = subtotal - descuento

        if self.__habitacion == "2" and self.__dias > "5":
            descuento = (habitacion1*0.10) - habitacion1
        elif self.__habitacion == "2" and self.__dias > "10":
            descuento = (habitacion1*0.15) - habitacion1
        elif self.__habitacion == "2" and self.__dias > "15":
            descuento = (habitacion1*0.20) - habitacion1
        else:
            descuento = 0
            subtotal = habitacion2*self.__dias
            total = subtotal - descuento
        
        if self.__habitacion == "3" and self.__dias > "5":
            descuento = (habitacion1*0.10) - habitacion1
        elif self.__habitacion == "3" and self.__dias > "10":
            descuento = (habitacion1*0.15) - habitacion1
        elif self.__habitacion == "3" and self.__dias > "15":
            descuento = (habitacion1*0.20) - habitacion1
        else:
            descuento = 0
            subtotal = habitacion3*self.__dias
            total = subtotal - descuento
        
        if self.__habitacion == "4" and self.__dias > "5":
            descuento = (habitacion1*0.10) - habitacion1
        elif self.__habitacion == "4" and self.__dias > "10":
            descuento = (habitacion1*0.15) - habitacion1
        elif self.__habitacion == "4" and self.__dias > "15":
            descuento = (habitacion1*0.20) - habitacion1
        else:
            descuento = 0
            subtotal = habitacion4*self.__dias
            total = subtotal - descuento
        
        if self.__habitacion == "5" and self.__dias > "5":
            descuento = (habitacion1*0.10) - habitacion1
        elif self.__habitacion == "5" and self.__dias > "10":
            descuento = (habitacion1*0.15) - habitacion1
        elif self.__habitacion == "5" and self.__dias > "15":
            descuento = (habitacion1*0.20) - habitacion1
        else:
            descuento = 0
            subtotal = habitacion5*self.__dias
            total = subtotal - descuento

        print("Su nombre es: {}".format(self.__nombre))
        print("Los dias en que se hospedo fueron: {}".format(self.__dias))
        print("La tarifa de la habitacion escogida fue de: {}".format(self.__habitacion1))
        print("El subtotal es de: {}".format(subtotal))
        print("El descuento es de: {}".format(descuento))
        print("El total a pagar es de: {}".format(total))


if __name__ == "__main__":
    cantidad_de_huespedes = int(input("Ingrese la cantidad de huespedes: "))
    nombre = input("Ingrese su nombre: ")
    apellido = input("Ingrese su apellido: ")
    edad =int( input("Ingrese su edad: "))
    habitacion = int(input("Ingrese su tipo de habitacion (1, 2, 3, 4, 5): "))
    dias = int(input("Ingresar los dias que desea hospedarse: "))
    guamuchil = Hotel(edad, habitacion, dias)
    guamuchil.descuento_hospedaje()